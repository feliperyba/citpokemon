//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataAcess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity;

    public partial class Pokemon
    {
        [Required(ErrorMessage = "Um ID � necess�rio!", AllowEmptyStrings = false)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Um Nome � necess�rio!", AllowEmptyStrings = false)]
        public string Name { get; set; }
        [Required(ErrorMessage = "Uma Imagem  � necess�ria!", AllowEmptyStrings = false)]
        public string ImagePath { get; set; }
        [Required(ErrorMessage = "� necess�rio informar se ja possui esse pokemon", AllowEmptyStrings = false)]
        public bool CurrentHave { get; set; }
        [ForeignKey("Id")]
        [Required(ErrorMessage = "� necess�rio informar o tipo desse pokemon", AllowEmptyStrings = false)]
        public int PokemonTypeId { get; set; }

        public string PokemonTypes { get; set; }
        public virtual ICollection<PokemonType> PokemonType { get; set; }
    }
}
