﻿
using PokemonGOWeb.Models;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using DataAcess;
using System.Web.Helpers;

namespace PokemonGOWeb.Controllers
{
    public class PokemonController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }

        //LIST FUNCTION//
        // get pokemon list using AngularJS and EF
        public JsonResult GetPokeList()
        {
            //creating the DBContext
            PokemonEntities pk;
            pk = new PokemonEntities();
            pk.Configuration.ProxyCreationEnabled = false;//to be able to serialize

            //Pokemon retrieve list
            List<Pokemon> Pokemon = new List<Pokemon>();

            //lambda query
            var query = (from p in pk.Pokemons
                         join t in pk.PokemonTypes
                         on p.PokemonTypeId equals t.Id
                         select new {
                             Id = p.Id,
                             Name = p.Name,
                             ImagePath = p.ImagePath,
                             CurrentHave = p.CurrentHave,
                             PokemonTypeId = p.PokemonTypeId,
                             Description = t.Description
                         }).ToList();

            //Adding the result to the final list
            foreach (var poke in query)
            {
                Pokemon pokeAux = new Pokemon();
                pokeAux.Id = poke.Id;
                pokeAux.ImagePath = poke.ImagePath;
                pokeAux.Name = poke.Name;
                pokeAux.PokemonTypeId = poke.PokemonTypeId;
                pokeAux.CurrentHave = poke.CurrentHave;
                pokeAux.PokemonTypes = poke.Description;
                Pokemon.Add(pokeAux);
            }
            //fetch results
            return new JsonResult { Data = Pokemon, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult GetPokeTypeList()
        {
            //creating the DBContext
            PokemonEntities pk;
            pk = new PokemonEntities();
            pk.Configuration.ProxyCreationEnabled = false;//to be able to serialize

            //Pokemon retrieve list
            List<PokemonType> PokeType = new List<PokemonType>();

            //lambda query
            var query = (from t in pk.PokemonTypes
                         select new
                         {
                             Id = t.Id,
                             Description = t.Description
                         }).ToList();

            //Adding the result to the final list
            foreach (var type in query)
            {
                PokemonType typeAux = new PokemonType();
                typeAux.Id = type.Id;
                typeAux.Description = type.Description;
                PokeType.Add(typeAux);
            }
            //fetch results
            return new JsonResult { Data = PokeType, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        //CREATE FUNCTION//
        [HttpPost]
        public ActionResult CreatePokemon(Pokemon pokemon)
        {
            //creating the DBContext
            PokemonEntities pk;
            pk = new PokemonEntities();
            pk.Configuration.ProxyCreationEnabled = false;//to be able to serialize

            //select to check if we will find the register on DB
            Pokemon pokeCreate = new Pokemon();
            //giving atributes values
            pokeCreate.ImagePath = pokemon.ImagePath;
            pokeCreate.Name = pokemon.Name;
            pokeCreate.PokemonTypeId = pokemon.PokemonTypeId;
            //adding to DB
            pk.Pokemons.Add(pokeCreate);
            pk.SaveChanges();

            return View("~/Views/Home/Index.cshtml");
        }

        //UPDATE FUNCTION//
        [HttpPost]
        public JsonResult UpdatePokemon(Pokemon pokemon)
        {
            //creating the DBContext
            PokemonEntities pk;
            pk = new PokemonEntities();
            pk.Configuration.ProxyCreationEnabled = false;//to be able to serialize

            //select to check if we will find the register on DB
            Pokemon pokeUpd = pk.Pokemons.Where(p => p.Id == pokemon.Id).FirstOrDefault();
            if (pokeUpd != null)
            {
                //Here we'll only update the CurrentHave field, but we could update the entire row if it's needed.
                pokeUpd.CurrentHave = !pokeUpd.CurrentHave;
                pk.SaveChanges();
                return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
            {
                return new JsonResult { Data = "Error", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
        //DELETE FUNCTION//
        [HttpPost]
        public JsonResult DeletePokemon(Pokemon pokemon)
        {
            //creating the DBContext
            PokemonEntities pk;
            pk = new PokemonEntities();
            pk.Configuration.ProxyCreationEnabled = false;//to be able to serialize

            //select to check if we will find the register on DB
            Pokemon pokeDel = pk.Pokemons.Where(p => p.Id == pokemon.Id).FirstOrDefault();
            if (pokeDel != null)
            {
                //Here we'll only delete the row.
                pk.Pokemons.Remove(pokeDel);
                pk.SaveChanges();
                return new JsonResult { Data = "Success", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
            else
            {
                return new JsonResult { Data = "Error", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
            }
        }
    }
}