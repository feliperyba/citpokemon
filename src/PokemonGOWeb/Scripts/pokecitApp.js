﻿//Angular module and controllers
//The best it's to create each JS file for each controller, but since we going to work only in one controller
//let make it simple
(function () {
    //create module
    var app = angular.module('pokecit', ["ngMessages", "ngRoute"]);

    //Create Controllers
    app.controller('HomeController', function ($scope) {
        //$scope.message = "this is a test";
    });
    //POKEMON CONTROLLER
    app.controller('PokemonController', ['$scope','PokeListService', function ($scope, PokeListService) {
        //this will receive our pokelist
        $scope.arrPokemons = null;
        $scope.arrPokemonsTypes = null;
        //
        //creation variables
        $scope.selectedType = null;
        $scope.pokeNome = null;
        $scope.pokeImg = null;
        //
        //
        //this will check who is checked or not
        $scope.arrCheckPokemons = null;
        //get the pokemon list
        PokeListService.GetPokeList().then(function (p) {
            $scope.arrPokemons = null;
            $scope.arrPokemons = p.data;
        }, function() {
            alert('Algo de errado aconteceu enquanto atualizavamos sua PokeDex =(');
        });

        $scope.loadType = function () {
            //get the type list
            PokeListService.GetPokeTypeList().then(function (p) {
                $scope.arrPokemonsTypes = null;
                $scope.arrPokemonsTypes = p.data;
            }, function () {
                alert('Algo de errado aconteceu enquanto listamos os tipos de pokemons. =(');
            });
        };
        //Create a new pokemon information.
        $scope.CreatePokemon = function () {
            var pokemon = {
                PokemonTypeId : $scope.selectedType,
                Name : $scope.pokeNome,
                ImagePath : $scope.pokeImg
            };
            PokeListService.CreatePokeData(pokemon).then(function (p) {
                //reseting form values after end
                //$scope.pokeForm.$setPristine();
                $scope.selectedType = null;
                $scope.pokeNome = null;
                $scope.pokeImg = null;
                alert("Cadastro de Pokemon efetuado com sucesso.");
                //get the pokemon list
                PokeListService.GetPokeList().then(function (p) {
                    $scope.arrPokemons = null;
                    $scope.arrPokemons = p.data;
                }, function () {
                    alert('Algo de errado aconteceu enquanto atualizavamos sua PokeDex =(');
                });
            }, function () {
                alert("Algo de errado aconteceu enquanto atualizavamos sua PokeDex. =(");
            });
        };
        //Update if you have or not this pokemon
        $scope.updatePokeCheck = function (pokemon) {
            PokeListService.updatePokeData(pokemon).then(function (p) {
                //get the pokemon list
                PokeListService.GetPokeList().then(function (p) {
                    $scope.arrPokemons = null;
                    $scope.arrPokemons = p.data;
                }, function () {
                    alert('Algo de errado aconteceu enquanto atualizavamos sua PokeDex =(');
                });
            }, function () {
                alert("Algo de errado aconteceu enquanto atualizavamos sua PokeDex. =(");
            });
        };
        //delete pokemon if user agree
        $scope.deletePokemon = function (pokemon) {
            PokeListService.deletePokeData(pokemon).then(function (p) {
                //get the pokemon list
                PokeListService.GetPokeList().then(function (p) {
                    $scope.arrPokemons = null;
                    $scope.arrPokemons = p.data;
                }, function () {
                    alert('Algo de errado aconteceu enquanto atualizavamos sua PokeDex =(');
                });
            }, function () {
                alert("Algo de errado aconteceu enquanto atualizavamos sua PokeDex. =(");
            });            
        };

    }])   //our service to retrieve the DB data.
        .factory('PokeListService', ['$http', function ($http) {
            //Get all pokemon list
            return {
                GetPokeList: function () {
                    return $http.get('/Pokemon/GetPokeList');
                },
                GetPokeTypeList: function () {
                    return $http.get('/Pokemon/GetPokeTypeList');
                },
                //Create Pokemon data and return the list after it.
                CreatePokeData: function (pokemon) {
                    var response = $http({
                        url: '/Pokemon/CreatePokemon',
                        method: 'POST',
                        data: pokemon
                    });
                    return response;
                },
                //Update Pokemon data and return the list after it.
                updatePokeData: function (pokemon) {
                    var response = $http({
                        url: '/Pokemon/UpdatePokemon',
                        method: 'POST',
                        data: pokemon
                    });
                    return response;
                },
                //delete Pokemon data and return the list after it.
                deletePokeData: function (pokemon) {
                    var response = $http({
                        url: '/Pokemon/DeletePokemon',
                        method: 'POST',
                        data: pokemon
                    });
                    return response;
                }
            };
        }]);

    //confirm window directive
    app.directive('ngConfirmClick', [
        function () {
            return {
                link: function (scope, element, attr) {
                    var msg = attr.ngConfirmClick || "Tem certeza que deseja deletar esse Pokemon?";
                    var clickAction = attr.confirmedClick;
                    element.bind('click', function (event) {
                        if (window.confirm(msg)) {
                            scope.$eval(clickAction);
                        }
                    });
                }
            };
        }]);
})();