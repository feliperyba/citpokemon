﻿using PokemonGOCore.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace PokemonGOCore.Infrastructure
{
    public class PokemonGOContext : DbContext
    {
        public PokemonGOContext()
            : base("PokemonGOContext")
        {

        }

        //public DbSet<PokemonCO> Pokemons { get; set; }
        //public DbSet<PokemonTypeCO> PokemonTypes { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}

